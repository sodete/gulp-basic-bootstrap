
/**
 * GLOBAL REQUIREMENTS
 */

try {
    var gulp = require('gulp');
    var del = require('del');
    var sass = require('gulp-sass');
    var browserSync = require('browser-sync');
    var autoprefixer = require('gulp-autoprefixer');
    var sourcemaps   = require('gulp-sourcemaps');
    var concat = require('gulp-concat');
    var checkFilesExist = require('check-files-exist');
    var stripCssComments = require('gulp-strip-css-comments');
    var uglify = require('gulp-uglify');
    var reload = browserSync.reload;
} catch (e) {
    // Unknown error, rethrow it.
    if (e.code !== "MODULE_NOT_FOUND") { throw e; }
    // Otherwise, we have a missing dependency. If the module is in the dependency list, the user just needs to run `npm install`.
    // Otherwise, they need to install and save it.
    var dependencies = require("./package.json").devDependencies;
    var module = e.toString().match(/'(.*?)'/)[1];
    var command = "npm install";
    if (typeof dependencies[module] === "undefined") { command += " --save-dev " + module; }
    console.error(e.toString() + ". Fix this by executing:\n\n" + command + "\n");
    process.exit(1);
}
/**
 * PATHS
 */

var paths = function () {
    var root = './assets';
    var src = root + '/src';
    var dest = root + '/dest';
    var libs = './node_modules';

    return {
        styles: {
            src: {
                project: src + '/styles',
                bootstrap: libs + '/bootstrap/scss'
            },
            dest: {
                project: dest + '/styles'
            }
        },
        scripts: {
            src: {
                project: [
                    src + '/scripts/**/*.js'
                ],
                libraries: [
                    libs + '/jquery/dist/jquery.js',
                    libs + '/popper.js/dist/umd/popper.js',
                    libs + '/bootstrap/dist/js/bootstrap.js'
                ]
            },
            dest: {
                project: dest + '/scripts'
            }
        },
        public: root + '/html'
    };

}();

/**
 * TASK: CLEAN
 */

gulp.task('clean:styles', function () {
    return del(paths.styles.dest.project);
});

gulp.task('clean:scripts', function () {
    return del(paths.scripts.dest.project);
});

/**
 * TASK: STYLES
 */

function compilerDevStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'expanded'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

function compileProdStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(stripCssComments({
                preserve: false
            }))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('styles', ['clean:styles'], function() {
    compilerDevStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

gulp.task('styles:prod', ['clean:styles'], function() {
    compileProdStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

/**
 * TASK: SCRIPTS
 */

function compilerDevScripts(src, dest) {
    checkFilesExist(paths.scripts.src.libraries, './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(concat('scripts.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}
function compilerProdScripts(src, dest) {
    checkFilesExist(paths.scripts.src.libraries, './').then(function () {
        return gulp.src(src)
            .pipe(concat('scripts.js'))
            .pipe(uglify())
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('scripts', ['clean:scripts'], function() {
    compilerDevScripts(paths.scripts.src.libraries.concat(paths.scripts.src.project), paths.scripts.dest.project)
});
gulp.task('scripts:prod', ['clean:scripts'], function() {
    compilerProdScripts(paths.scripts.src.libraries.concat(paths.scripts.src.project), paths.scripts.dest.project)
});

/**
 * TASK: BROWSER
 */

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./",
            directory: true
        }
    });
    return gulp.watch([
        paths.public + '/**/*.html',
        paths.styles.dest.project + '/**/*.css',
        paths.scripts.dest.project + '/**/*.js'
    ]).on("change", reload);
});

/**
 * TASK: WATCH
 */

gulp.task('watch-styles', function() {
    return gulp.watch([
        paths.styles.src.project + '/**/*.scss'
    ], ['styles']);
});

gulp.task('watch-scripts', function() {
    return gulp.watch([
        paths.scripts.src.project + '/**/*.js'
    ], ['scripts']);
});

gulp.task('watch', ['watch-styles', 'watch-scripts']);

/**
 * TASK: DEVELOP
 */

gulp.task('develop', ['serve','watch','build:dev']);

/**
 * TASK: BUILD PROD
 */

gulp.task('build:prod', ['styles:prod','scripts:prod']);
/**
 * TASK: BUILD DEV
 */

gulp.task('build:dev', ['styles','scripts']);

/**
 * TASK: DEFAULT
 */

gulp.task('default', ['build:dev']);