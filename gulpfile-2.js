
/**
 * GLOBAL REQUIREMENTS
 */

try {
    var gulp = require('gulp');
    var del = require('del');
    var sass = require('gulp-sass');
    var browserSync = require('browser-sync');
    var autoprefixer = require('gulp-autoprefixer');
    var sourcemaps = require('gulp-sourcemaps');
    var concat = require('gulp-concat');
    var checkFilesExist = require('check-files-exist');
    var stripCssComments = require('gulp-strip-css-comments');
    var uglify = require('gulp-uglify');
    var imagemin = require('gulp-imagemin');
    var twig = require('gulp-twig');
    var htmlbeautify = require('gulp-html-beautify');
    var cachebust = require('gulp-cache-bust');
    var htmlPartial = require('./assets/src/mocks/scripts/gulp-html-partial');
    var reload = browserSync.reload;
} catch (e) {
    // Unknown error, rethrow it.
    if (e.code !== "MODULE_NOT_FOUND") { throw e; }
    // Otherwise, we have a missing dependency. If the module is in the dependency list, the user just needs to run `npm install`.
    // Otherwise, they need to install and save it.
    var dependencies = require("./package.json").devDependencies;
    var module = e.toString().match(/'(.*?)'/)[1];
    var command = "npm install";
    if (typeof dependencies[module] === "undefined") { command += " --save-dev " + module; }
    console.error(e.toString() + ". Fix this by executing:\n\n" + command + "\n");
    process.exit(1);
}

/**
 * PATHS
 */

var paths = function () {
    var root = './assets';
    var src = root + '/src';
    var dest = root + '/dest';
    var libs = './node_modules';

    return {
        styles: {
            src: {
                project: src + '/styles',
                bootstrap: libs + '/bootstrap/scss'
            },
            dest: {
                project: dest + '/styles'
            }
        },
        scripts: {
            src: {
                project: [
                    src + '/scripts/**/*.js'
                ],
                libraries: [
                    libs + '/jquery/dist/jquery.js',
                    libs + '/jquery-ui-dist/jquery-ui.js',
                    libs + '/popper.js/dist/umd/popper.js',
                    libs + '/bootstrap/dist/js/bootstrap.js',
                    libs + '/bootstrap-select/dist/js/bootstrap-select.js',
                    libs + '/mobile-detect/mobile-detect.min.js',
                    libs + '/jquery-datepicker/jquery-datepicker.js',
                    libs + '/bxslider/dist/vendor/jquery.easing.1.3.js',
                    libs + '/bxslider/dist/jquery.bxslider.js'
                ]
            },
            dest: {
                project: dest + '/scripts'
            }
        },
        common: {
            src: {
                fonts: src + '/fonts/**/*',
                images: src + '/images/**/*',
                videos: src + '/videos/**/*'
            },
            dest: {
                fonts: dest + '/fonts',
                images: dest + '/images',
                videos: dest + '/videos'
            }
        },
        mocks: {
            src: {
                html: src + '/mocks/**/*.html',
                part: src + '/mocks/partials/'
            },
            dest: {
                html: dest + '/mocks/'
            }
        },
        twig: {
            src: {
                html: src + '/twig/**/*.twig'
            },
            dest: {
                html: dest + '/twig/'
            }
        }
    };

}();

/**
 * TASK: CLEAN
 */

gulp.task('clean:common', function () {
    return del([
        paths.common.dest.fonts,
        paths.common.dest.images,
        paths.common.dest.videos
    ]);
});

gulp.task('clean:styles', function () {
    return del(paths.styles.dest.project);
});

gulp.task('clean:scripts', function () {
    return del(paths.scripts.dest.project);
});

gulp.task('clean:mocks', function () {
    return del(paths.mocks.dest.html);
});

gulp.task('clean:twig', function () {
    return del(paths.twig.dest.html);
});

/**
 * TASK: COMMON
 */

function commonFonts(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonVideos(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonImagesDev(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonImagesProd(src, dest) {
    return gulp.src(src)
        .pipe(imagemin())
        .pipe(gulp.dest(dest));
}

gulp.task('common', ['clean:common'], function () {
    commonFonts(paths.common.src.fonts, paths.common.dest.fonts);
    commonVideos(paths.common.src.videos, paths.common.dest.videos);
    return commonImagesDev(paths.common.src.images, paths.common.dest.images);
});

gulp.task('common:prod', ['clean:common'], function () {
    commonFonts(paths.common.src.fonts, paths.common.dest.fonts);
    commonVideos(paths.common.src.videos, paths.common.dest.videos);
    return commonImagesProd(paths.common.src.images, paths.common.dest.images);
});

/**
 * TASK: STYLES
 */

function compilerDevStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'expanded'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

function compileProdStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(stripCssComments({
                preserve: false
            }))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('styles', ['clean:styles'], function () {
    compilerDevStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

gulp.task('styles:prod', ['clean:styles'], function () {
    compileProdStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

/**
 * TASK: SCRIPTS
 */

function compilerDevScripts(src, dest) {
    checkFilesExist(paths.scripts.src.libraries, './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(concat('scripts.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}
function compilerProdScripts(src, dest) {
    checkFilesExist(paths.scripts.src.libraries, './').then(function () {
        return gulp.src(src)
            .pipe(concat('scripts.js'))
            .pipe(uglify())
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('scripts', ['clean:scripts'], function () {
    compilerDevScripts(paths.scripts.src.libraries.concat(paths.scripts.src.project), paths.scripts.dest.project)
});
gulp.task('scripts:prod', ['clean:scripts'], function () {
    compilerProdScripts(paths.scripts.src.libraries.concat(paths.scripts.src.project), paths.scripts.dest.project)
});

/**
 * TASK: HTML
 */

gulp.task('mocks', ['clean:mocks'], function () {
    gulp.src([paths.mocks.src.html])
        .pipe(htmlPartial({
            basePath: paths.mocks.src.part
        }))
        .pipe(gulp.dest(paths.mocks.dest.html));
});

/**
 * TASK: TWIG
 */

gulp.task('twig', ['clean:twig'], function () {
    var options = {
        "indent_with_tabs": true,
        "indent_inner_html": true,
        "wrap_line_length": 0
    }
    gulp.src([paths.twig.src.html])
        .pipe(twig())
        .pipe(cachebust({
            type: 'timestamp'
        }))
        .pipe(htmlbeautify(options))
        .pipe(gulp.dest(paths.twig.dest.html));
});

/**
 * TASK: BROWSER
 */

gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: "./",
            directory: true
        }
    });
    return gulp.watch([
        paths.styles.dest.project + '/**/*.css',
        paths.scripts.dest.project + '/**/*.js',
        paths.mocks.dest.html + '/**/*.html',
        paths.twig.dest.html + '/**/*.html'
    ]).on("change", reload);
});

/**
 * TASK: WATCH
 */

gulp.task('watch-styles', function () {
    return gulp.watch([
        paths.styles.src.project + '/**/*.scss'
    ], ['styles']);
});

gulp.task('watch-scripts', function () {
    return gulp.watch([
        paths.scripts.src.project + '/**/*.js'
    ], ['scripts']);
});

gulp.task('watch-mocks', function () {
    return gulp.watch([
        paths.mocks.src.html
    ], ['mocks']);
});

gulp.task('watch-twig', function () {
    return gulp.watch([
        paths.twig.src.html
    ], ['twig']);
});

gulp.task('watch', ['watch-styles', 'watch-scripts', 'watch-mocks', 'watch-twig']);

/**
 * TASK: DEVELOP
 */

gulp.task('develop', ['build:dev','server','watch']);

/**
 * TASK: BUILD PROD
 */

gulp.task('build:prod', ['common:prod','styles:prod','scripts:prod']);

/**
 * TASK: BUILD DEV
 */

gulp.task('build:dev', ['common', 'styles','scripts','mocks','twig']);

/**
 * TASK: DEFAULT
 */

gulp.task('default', ['build:dev']);