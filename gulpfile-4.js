
/**
 * GLOBAL REQUIREMENTS
 */

try {
    var gulp = require('gulp');
    var archiver = require('archiver');
    var fs = require('fs');
    var del = require('del');
    var sass = require('gulp-sass');
    var browserSync = require('browser-sync');
    var autoprefixer = require('gulp-autoprefixer');
    var sourcemaps = require('gulp-sourcemaps');
    var concat = require('gulp-concat');
    var checkFilesExist = require('check-files-exist');
    var stripCssComments = require('gulp-strip-css-comments');
    var uglify = require('gulp-uglify');
    var imagemin = require('gulp-imagemin');
    var soynode = require('gulp-soynode');
    var replace = require('gulp-replace');
    var cssUrlAdjuster 	= require('gulp-css-url-adjuster');
    var htmlbeautify = require('gulp-html-beautify');
    var htmlPartial = require('gulp-html-partial');
    var cachebust = require('gulp-cache-bust');
    var replace = require('gulp-replace');
    var reload = browserSync.reload;
    var deployer = require('nexus-deployer');
    var packageJson = require('./package.json');
    var log = require('fancy-log');
    var homedir = require('homedir');
    var PropertiesReader = require('properties-reader');
    var sh = require('child_process').exec;
    var jsonfile = require('jsonfile');
    var _path = require('path');
    // Javascript
    var babel = require('gulp-babel');
} catch (e) {
    // Unknown error, rethrow it.
    if (e.code !== "MODULE_NOT_FOUND") { throw e; }
    // Otherwise, we have a missing dependency. If the module is in the dependency list, the user just needs to run `npm install`.
    // Otherwise, they need to install and save it.
    var dependencies = require("./package.json").devDependencies;
    var module = e.toString().match(/'(.*?)'/)[1];
    var command = "npm install";
    if (typeof dependencies[module] === "undefined") { command += " --save-dev " + module; }
    console.error(e.toString() + ". Fix this by executing:\n\n" + command + "\n");
    process.exit(1);
}

/**
 * PATHS & OPTIONS
 */

var isProduction = process.env.NODE_ENV === 'production';
var extension = isProduction ? '.min.js' : '.js';

var paths = function () {
    var src = './src';
    var target = 'target';
    var dest = target + '/dest';
    var libs = './node_modules';

    return {
        target:target,
        dest:dest,
        styles: {
            src: {
                project: src + '/styles',
                bootstrap: libs + '/bootstrap/scss'
            },
            dest: {
                project: dest + '/styles'
            }
        },
        scripts: {
            src: {
                redesign: [
                    src + '/scripts/utils/utils.js',
                    src + '/scripts/utils/web-storage.js',
                    src + '/scripts/services/http-service.js',
                    src + '/scripts/services/site-search-service.js',
                    src + '/scripts/global/global.js',
                    src + '/scripts/global/events.js',
                    src + '/scripts/global/bxslider.js',
                    src + '/scripts/global/components.js',
                    src + '/scripts/global/modules.js'
                ],
                project: [
                    src + '/scripts/utils/utils.js',
                    src + '/scripts/utils/web-storage.js',
                    src + '/scripts/services/http-service.js',
                    src + '/scripts/services/site-search-service.js',
                    src + '/scripts/global/global.js',
                    src + '/scripts/global/events.js',
                    src + '/scripts/global/bxslider.js',
                    src + '/scripts/global/components.js',
                    src + '/scripts/global/modules.js',
                    src + '/scripts/vuejs/services/axios-service.js',
                    src + '/scripts/vuejs/search-filter/search-filter.js'
                ],
                libraries: [
                    libs + '/jquery/dist/jquery.js',
                    libs + '/jquery-ui-dist/jquery-ui.js',
                    libs + '/popper.js/dist/umd/popper.js',
                    libs + '/bootstrap/dist/js/bootstrap.js',
                    libs + '/bootstrap-select/dist/js/bootstrap-select.js',
                    libs + '/mobile-detect/mobile-detect.min.js',
                    libs + '/jquery-datepicker/jquery-datepicker.js',
                    libs + '/bxslider/dist/vendor/jquery.easing.1.3.js',
                    libs + '/bxslider/dist/jquery.bxslider.js',
                    libs + '/js-cookie/src/js.cookie.js',
                    libs + '/vue/dist/vue' + extension,
                    libs + '/axios/dist/axios.js'
                ],
                polyfills: [
                    libs + '/babel-polyfill/dist/polyfill' + extension
                ]
            },
            dest: {
                project: dest + '/scripts'
            }
        },
        common: {
            src: {
                api: src + '/api/**/*',
                fonts: src + '/fonts/**/*',
                images: src + '/images/**/*',
                videos: src + '/videos/**/*'
            },
            dest: {
                api: dest + '/api',
                fonts: dest + '/fonts',
                images: dest + '/images',
                videos: dest + '/videos'
            }
        },
        mocks: {
            src: {
                html: src + '/mocks/**/*.html',
                part: src + '/mocks/includes/'
            },
            dest: {
                html: dest + '/mocks/'
            }
        },
        closure: {
            src: {
                html: src + '/closure/**/*.soy'
            },
            dest: {
                html: dest + '/closure/'
            }
        }
    };

}();

var options = {
    partial: {
        basePath: paths.mocks.src.part,
        tagName: 'include',
        variablePrefix: '$'
    },
    beautify: {
        "indent_with_tabs": true,
        "indent_inner_html": true,
        "wrap_line_length": 0
    },
    cache: {
        type: 'timestamp'
    }
}

/**
 * TASK: CLEAN
 */

gulp.task('clean', ['clean:common', 'clean:styles', 'clean:scripts', 'clean:mocks', 'clean:closure', 'clean:target'], function () {});

gulp.task('clean:target', function () {
    return del(paths.target);
});

gulp.task('clean:common', function () {
    return del([
        paths.common.dest.api,
        paths.common.dest.fonts,
        paths.common.dest.images,
        paths.common.dest.videos
    ]);
});

gulp.task('clean:styles', function () {
    return del(paths.styles.dest.project);
});

gulp.task('clean:scripts', function () {
    return del(paths.scripts.dest.project);
});

gulp.task('clean:mocks', function () {
    return del(paths.mocks.dest.html);
});

gulp.task('clean:closure', function () {
    return del(paths.closure.dest.html);
});

/**
 * TASK: COMMON
 */

function commonFakeApi(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonFonts(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonVideos(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonImagesDev(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonImagesPro(src, dest) {
    return gulp.src(src)
        .pipe(imagemin())
        .pipe(gulp.dest(dest));
}

gulp.task('common:dev', ['clean:common'], function () {
    commonFakeApi(paths.common.src.api, paths.common.dest.api);
    commonFonts(paths.common.src.fonts, paths.common.dest.fonts);
    commonVideos(paths.common.src.videos, paths.common.dest.videos);
    return commonImagesDev(paths.common.src.images, paths.common.dest.images);
});

gulp.task('common:pro', ['clean:common'], function () {
    commonFakeApi(paths.common.src.api, paths.common.dest.api);
    commonFonts(paths.common.src.fonts, paths.common.dest.fonts);
    commonVideos(paths.common.src.videos, paths.common.dest.videos);
    return commonImagesPro(paths.common.src.images, paths.common.dest.images);
});

/**
 * TASK: STYLES
 */

function compilerDevStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'expanded'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

function compileProStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(stripCssComments({
                preserve: false
            }))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

function executeUrlAdjuster(value1,value2) {
    return cssUrlAdjuster({
        replace:  [value1,value2]
    });
}

function compilerRedesignStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'expanded'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(cssUrlAdjuster({
                replace:  ['../fonts','/assets/v1/fonts']
            }))
            .pipe(executeUrlAdjuster('../fonts','/assets/v1/fonts'))
            .pipe(executeUrlAdjuster('../images','/assets/v1/images'))
            .pipe(replace('> .redesign .listing_item', '> .listing_item'))
            .pipe(replace('.bg-dark .redesign', '.redesign .bg-dark'))
            .pipe(replace('.bg-dark .redesign', '.redesign .bg-medium'))
            .pipe(replace('.bg-dark .redesign', '.redesign .bg-light'))
            .pipe(replace('.redesign .drophover_menu', '.drophover_menu'))
            .pipe(replace('.redesign .drophover', '.drophover'))
            .pipe(replace('.redesign .-clearDark', '.-clearDark'))
            .pipe(replace('.-clearDark .redesign .linkNav', '.-clearDark .linkNav'))
            .pipe(replace('.open .redesign .header_desktop_wrapper', '.open .header_desktop_wrapper'))
            .pipe(replace('.redesign .m15.show .redesign .m15_wrap', '.redesign .m15.show .m15_wrap'))
            .pipe(replace('.show .redesign .c13_footer', '.show .c13_footer'))
            .pipe(replace('.bg-medium .dropdown.show .redesign .dropdown-toggle', '.redesign .bg-medium .dropdown.show .dropdown-toggle'))
            .pipe(replace('.bg-dark .dropdown.show .redesign .dropdown-toggle', '.redesign .bg-dark .dropdown.show .dropdown-toggle'))
            .pipe(replace('.dropdown.show .redesign .dropdown-toggle', '.redesign .dropdown.show .dropdown-toggle'))

            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('styles:dev', ['clean:styles'], function () {
    compilerDevStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

gulp.task('styles:pro', ['clean:styles'], function () {
    compileProStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

/**
 * TASK: HEADER & FOOTER FOR OLD SITE
 */

gulp.task('styles:redesign', ['clean:styles'], function () {
    return compilerRedesignStyles(paths.styles.src.project + '/redesign.scss', paths.styles.dest.project)
});

/**
 * TASK: SCRIPTS
 */

function compilerLibsScripts(src) {
    return checkFilesExist(src, './').then(function () {
        return gulp.src(src)
            .pipe(concat('libraries.js'))
            .pipe(uglify())
            .pipe(gulp.dest(paths.scripts.dest.project));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}
function compilerDevScripts(src, dest, name) {
    return checkFilesExist(src, './').then(function () {
        return gulp.src(src.concat(paths.scripts.src.polyfills))
            .pipe(sourcemaps.init())
            .pipe(concat(name))
            .pipe(babel())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}
function compilerProScripts(src, dest, name) {
    return checkFilesExist(src, './').then(function () {
        return gulp.src(src.concat(paths.scripts.src.polyfills))
            .pipe(concat(name))
            .pipe(babel())
            .pipe(uglify())
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err);
        process.exit();
    });
}

gulp.task('scripts:dev', ['clean:scripts'], function () {
    return Promise.all([
        compilerLibsScripts(paths.scripts.src.libraries),
        compilerDevScripts(paths.scripts.src.project, paths.scripts.dest.project, 'scripts.js'),
        compilerDevScripts(paths.scripts.src.redesign, paths.scripts.dest.project, 'redesign.js')
    ]);
});
gulp.task('scripts:pro', ['clean:scripts'], function () {
    return Promise.all([
        compilerLibsScripts(paths.scripts.src.libraries),
        compilerProScripts(paths.scripts.src.project, paths.scripts.dest.project, 'scripts.js'),
        compilerProScripts(paths.scripts.src.redesign, paths.scripts.dest.project, 'redesign.js')
    ]);
});

/**
 * TASK: HTML
 */

gulp.task('mocks', ['clean:mocks'], function () {
    return gulp.src([paths.mocks.src.html])
        .pipe(htmlPartial(options.partial))
        .pipe(htmlbeautify(options.beautify))
        .pipe(cachebust(options.cache))
        .pipe(gulp.dest(paths.mocks.dest.html));
});

/**
 * TASK: CLOSURE
 */

gulp.task('closure', ['clean:closure'], function () {
    return gulp.src([paths.closure.src.html])
        .pipe(soynode({renderSoyWeb: true}))
        .pipe(htmlbeautify(options.beautify))
        .pipe(gulp.dest(paths.closure.dest.html));
});

/**
 * TASK: BROWSER
 */

gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: paths.dest,
            directory: true
        },
        port: 3000,
        middleware: function(req,res,next) {
            if (req.headers.host === 'localhost:3000') {
                //automatic redirect to local.ncl.com (must have correct HAProxy settings pointing /assets/v1 to 127.0.0.1:<port>)
                res.writeHead(302, {
                    'Location': 'https://local.ncl.com' + req.url
                });
                res.end();
            } else {
                //remove /assets/v1 from incoming requests which will serve from baseDir (assets/dest)
                var pathToReplace = '/assets/v1';
                if (req.url.startsWith(pathToReplace)) {
                    req.url = req.url.length > pathToReplace.length ? req.url.substr(pathToReplace.length) : '/';
                }
            }
            return next();
        },
        startPath: "assets/v1"
    });
    return gulp.watch([
        paths.styles.dest.project + '/**/*.css',
        paths.scripts.dest.project + '/**/*.js',
        paths.mocks.dest.html + '/**/*.html',
        paths.closure.dest.html + '/**/*.html'
    ]).on("change", reload);
});

/**
 * TASK: WATCH
 */

gulp.task('watch-styles', function () {
    return gulp.watch([
        paths.styles.src.project + '/**/*.scss'
    ], ['styles:dev','styles:redesign']);
});

gulp.task('watch-scripts', function () {
    return gulp.watch([
        paths.scripts.src.project
    ], ['scripts:dev']);
});

gulp.task('watch-mocks', function () {
    return gulp.watch([
        paths.mocks.src.html
    ], ['mocks']);
});

gulp.task('watch-closure', function () {
    return gulp.watch([
        paths.closure.src.html
    ], ['closure']);
});

gulp.task('watch', ['watch-styles', 'watch-scripts','watch-mocks','watch-closure']);

/**
 * TASK: BUILD LIVE
 */

gulp.task('build:live', ['build:dev','watch','server']);

/**
 * TASK: BUILD PRO
 */

gulp.task('build:pro', ['common:pro','styles:pro','scripts:pro','build:html','styles:redesign']);

/**
 * TASK: BUILD DEV
 */

gulp.task('build:dev', ['common:dev', 'styles:dev','scripts:dev','build:html','styles:redesign']);

/**
 * TASK: BUILD MOCKS
 */

gulp.task('build:html', ['mocks','closure']);

/**
 * TASK: DEFAULT
 */

gulp.task('default', ['build:dev']);

function repoURL() {
    if(/SNAPSHOT/.test(packageJson.version)){
        return 'http://nexus.nclmiami.ncl.com/content/repositories/snapshots';
    }else {
        return 'http://nexus.nclmiami.ncl.com/content/repositories/releases';
    }
}

function filename() { return packageJson.artifactId + '-' + packageJson.version; }
function getBranch(cb) {
    sh('git rev-parse --abbrev-ref HEAD', function (err, stdout, stderr) {
        var branch = stdout;
        if (branch === "HEAD") {
            sh('git branch --remote --verbose --no-abbrev --contains | sed -rne \'s/^[^\\\\/]*\\\\/([^\\\\ ]+).*\\$/\\\\1/p\'', function (err, stdout, stderr) {
                branch = stdout;
            });
        }
        cb(branch.trim());
    });
}
function getRevision(cb) {
    sh('git log -n1 --format=%H', function (err, stdout, stderr) { cb(stdout); });
}

function mkDir(targetDir) {
    sh('mkdir -p '+ targetDir, function (err, stdout, stderr) {
        if(stderr != ""){ log('mkDir error: '+ stderror);}
    });
}

function writeJson(version, branch, revision) {
    var dashboardApi = './' + paths.dest + '/tools/api';
    if (!fs.existsSync(dashboardApi)){ mkDir(dashboardApi); }
    jsonfile.writeFileSync(dashboardApi.replace("./", "") + "/dashBoard", {
        branch: branch,
        revision: revision,
        version: version
    });
}

gulp.task('versionJson', function () {
    var version = packageJson.version;
    getRevision(function (revision) {
        getBranch(function (branch) {
            return writeJson(version, branch, revision);
        });
    });
});

function repoURL() {
    if(/SNAPSHOT/.test(packageJson.version)){ return 'http://nexus.nclmiami.ncl.com/content/repositories/snapshots'; }
    else { return 'http://nexus.nclmiami.ncl.com/content/repositories/releases'; }
}

gulp.task('package', ['build:pro', 'versionJson'], function () {
    var buildDir = './' + paths.target;
    var file = buildDir.replace(".", "") + '/' + filename() + '.zip';
    if (!fs.existsSync(buildDir)){ mkDir(buildDir); }
    var output = fs.createWriteStream(__dirname + file);
    var archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });
    // listen for all archive data to be written
    // 'close' event is fired only when a file descriptor is involved
    output.on('close', function() {
        console.log(archive.pointer() + ' bytes written to ' + file);
    });

// This event is fired when the data source is drained no matter what was the data source.
// It is not part of this library but rather from the NodeJS Stream API.
// @see: https://nodejs.org/api/stream.html#stream_event_end
    output.on('end', function() { console.log('Data has been drained'); });

// good practice to catch warnings (ie stat failures and other non-blocking errors)
    archive.on('warning', function(err) {
        if (err.code === 'ENOENT') { console.log('ENOENT: ' + err.message); }
        else { throw err; }
    });

// good practice to catch this error explicitly
    archive.on('error', function(err) { throw err; });

// pipe archive data to the file
    archive.pipe(output);

    // append files from a sub-directory, putting its contents at the root of archive
    archive.directory(paths.dest, false);
    return archive.finalize();
});

gulp.task('publish', ['package'], function() {
    var properties = PropertiesReader(homedir() + "/.sbt/.ncl-nexus");
    var zipFile = paths.target + '/' + filename() + '.zip';
    log('zip: ' + zipFile);
    deployer.deploy({
        groupId: packageJson.groupId,
        artifactId: packageJson.artifactId,
        version: packageJson.version,
        packaging: 'zip',
        auth: {username: properties.get('user'), password: properties.get('password')},
        pomDir: paths.target + '/pom',
        url: repoURL(),
        artifact: zipFile
    }, function () {});
});