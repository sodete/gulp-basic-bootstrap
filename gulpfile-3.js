
/**
 * GLOBAL REQUIREMENTS
 */

try {
    // Globals
    var gulp = require('gulp');
    var del = require('del');
    var uglify = require('gulp-uglify');
    var concat = require('gulp-concat');
    var runSequence = require('run-sequence');
    var checkFilesExist = require('check-files-exist');
    var replace = require('gulp-replace');

    // Javascript
    var babel = require('gulp-babel');

    // Styles
    var sass = require('gulp-sass');
    var browserSync = require('browser-sync');
    var reload = browserSync.reload;
    var autoprefixer = require('gulp-autoprefixer');
    var sourcemaps = require('gulp-sourcemaps');
    var stripCssComments = require('gulp-strip-css-comments');
    var imagemin = require('gulp-imagemin');
    var soynode = require('gulp-soynode');
    var htmlbeautify = require('gulp-html-beautify');
    var cachebust = require('gulp-cache-bust');
    var htmlPartial = require('gulp-html-partial');

} catch (e) {
    // Unknown error, rethrow it.
    if (e.code !== "MODULE_NOT_FOUND") { throw e; }
    // Otherwise, we have a missing dependency. If the module is in the dependency list, the user just needs to run `npm install`.
    // Otherwise, they need to install and save it.
    var dependencies = require("./package.json").devDependencies;
    var module = e.toString().match(/'(.*?)'/)[1];
    var command = "npm install";
    if (typeof dependencies[module] === "undefined") { command += " --save-dev " + module; }
    console.error(e.toString() + ". Fix this by executing:\n\n" + command + "\n");
    process.exit(1);
}

/**
 * PATHS & OPTIONS
 */

var isProduction = process.env.NODE_ENV === 'production';
var extension = isProduction ? '.min.js' : '.js';

var paths = function () {
    var root = './assets';
    var src = root + '/src';
    var dest = root + '/dest';
    var libs = './node_modules';

    return {
        styles: {
            src: {
                project: src + '/styles',
                bootstrap: libs + '/bootstrap/scss'
            },
            dest: {
                project: dest + '/styles'
            }
        },
        scripts: {
            src: {
                project: [
                    src + '/scripts/global/global.js',
                    src + '/scripts/global/bxslider.js',
                    src + '/scripts/global/_affix.js',
                    src + '/scripts/global/utils.js',
                    src + '/scripts/global/guide.js',
                    src + '/scripts/global/components.js',
                    src + '/scripts/global/modules.js',
                    src + '/scripts/vuejs/services/http-service.js',
                    src + '/scripts/vuejs/search-filter/search-filter.js'
                ],
                libraries: [
                    libs + '/jquery/dist/jquery.js',
                    libs + '/jquery-ui-dist/jquery-ui.js',
                    libs + '/popper.js/dist/umd/popper.js',
                    libs + '/bootstrap/dist/js/bootstrap.js',
                    libs + '/bootstrap-select/dist/js/bootstrap-select.js',
                    libs + '/mobile-detect/mobile-detect.min.js',
                    libs + '/jquery-datepicker/jquery-datepicker.js',
                    libs + '/bxslider/dist/vendor/jquery.easing.1.3.js',
                    libs + '/bxslider/dist/jquery.bxslider.js',
                    libs + '/vue/dist/vue' + extension,
                    libs + '/axios/dist/axios.js',
                    libs + '/@vimeo/player/dist/player.js'
                ]
            },
            dest: {
                project: dest + '/scripts'
            }
        },
        common: {
            src: {
                api: src + '/api/**/*',
                fonts: src + '/fonts/**/*',
                images: src + '/images/**/*',
                videos: src + '/videos/**/*'
            },
            dest: {
                api: dest + '/api',
                fonts: dest + '/fonts',
                images: dest + '/images',
                videos: dest + '/videos'
            }
        },
        mocks: {
            src: {
                html: src + '/mocks/**/*.html',
                part: src + '/mocks/partials/'
            },
            dest: {
                html: dest + '/mocks/'
            }
        },
        closure: {
            src: {
                html: src + '/closure/**/*.soy'
            },
            dest: {
                html: dest + '/closure/'
            }
        }
    };

}();

var options = {
    partial: {
        basePath: paths.mocks.src.part,
        tagName: 'partial',
        variablePrefix: '@@'
    },
    beautify: {
        "indent_with_tabs": true,
        "indent_inner_html": true,
        "wrap_line_length": 0
    },
    cache: {
        type: 'timestamp'
    }
}

/**
 * TASK: CLEAN
 */

gulp.task('clean:common', function () {
    return del([
        paths.common.dest.api,
        paths.common.dest.fonts,
        paths.common.dest.images,
        paths.common.dest.videos
    ]);
});

gulp.task('clean:styles', function () {
    return del(paths.styles.dest.project);
});

gulp.task('clean:scripts', function () {
    return del(paths.scripts.dest.project);
});

gulp.task('clean:mocks', function () {
    return del(paths.mocks.dest.html);
});

gulp.task('clean:closure', function () {
    return del(paths.closure.dest.html);
});

/**
 * TASK: COMMON
 */

function commonJson(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonFonts(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonVideos(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonImagesDev(src, dest) {
    return gulp.src(src).pipe(gulp.dest(dest));
}

function commonImagesPro(src, dest) {
    return gulp.src(src)
        .pipe(imagemin())
        .pipe(gulp.dest(dest));
}

gulp.task('common:dev', ['clean:common'], function () {
    commonJson(paths.common.src.api, paths.common.dest.api);
    commonFonts(paths.common.src.fonts, paths.common.dest.fonts);
    commonVideos(paths.common.src.videos, paths.common.dest.videos);
    return commonImagesDev(paths.common.src.images, paths.common.dest.images);
});

gulp.task('common:pro', ['clean:common'], function () {
    commonJson(paths.common.src.api, paths.common.dest.api);
    commonFonts(paths.common.src.fonts, paths.common.dest.fonts);
    commonVideos(paths.common.src.videos, paths.common.dest.videos);
    return commonImagesPro(paths.common.src.images, paths.common.dest.images);
});

/**
 * TASK: STYLES
 */

function compilerDevStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'expanded'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

function compilerProStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'compressed'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(stripCssComments({
                preserve: false
            }))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('styles:dev', ['clean:styles'], function () {
    return compilerDevStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

gulp.task('styles:pro', ['clean:styles'], function () {
    return compilerProStyles(paths.styles.src.project + '/main.scss', paths.styles.dest.project)
});

/**
 * TASK: HEADER & FOOTER FOR OLD SITE
 */

function compilerRedesignStyles(src, dest) {
    checkFilesExist([paths.styles.src.bootstrap], './').then(function () {
        return gulp.src(src)
            .pipe(sass({
                includePaths: [
                    paths.styles.src.bootstrap
                ],
                outputStyle: 'expanded'
            }).on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions']
            }))
            .pipe(replace('../fonts','/assets/v1/fonts'))
            .pipe(replace('../images','/assets/v1/images'))
            .pipe(replace('.bg-primary .redesign a','.redesign .bg-primary a'))
            .pipe(replace('.redesign .mainMenu_item.active .redesign .mainMenu_link','.redesign .mainMenu_item.active .mainMenu_link'))
            .pipe(gulp.dest(dest));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('styles:redesign', function () {
    return compilerRedesignStyles(paths.styles.src.project + '/redesign.scss', paths.styles.dest.project)
});

/**
 * TASK: SCRIPTS
 */


function compilerLibsScripts(src) {
    return checkFilesExist(src, './').then(function () {
        return gulp.src(src)
            .pipe(concat('libraries.js'))
            .pipe(uglify())
            .pipe(gulp.dest(paths.scripts.dest.project));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

function compilerDevScripts(src) {
    return checkFilesExist(src, './').then(function () {
        return gulp.src(src)
            .pipe(sourcemaps.init())
            .pipe(babel())
            .pipe(concat('scripts.js'))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(paths.scripts.dest.project));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}
function compilerProScripts(src) {
    checkFilesExist(paths.scripts.src.libraries, './').then(function () {
        return gulp.src(src)
            .pipe(babel())
            .pipe(concat('scripts.js'))
            .pipe(uglify())
            .pipe(gulp.dest(paths.scripts.dest.project));
    }, function (err) {
        console.log(err.message);
        process.exit();
    });
}

gulp.task('scripts:dev', ['clean:scripts'], function () {
    return compilerLibsScripts(paths.scripts.src.libraries).then(function () {
        return compilerDevScripts(paths.scripts.src.project)
    })
});
gulp.task('scripts:pro', ['clean:scripts'], function () {
    return compilerLibsScripts(paths.scripts.src.libraries).then(function () {
        return compilerProScripts(paths.scripts.src.project)
    })
});

/**
 * TASK: HTML
 */

gulp.task('mocks', ['clean:mocks'], function () {
    return gulp.src([paths.mocks.src.html])
        .pipe(htmlPartial(options.partial))
        .pipe(htmlbeautify(options.beautify))
        .pipe(cachebust(options.cache))
        .pipe(gulp.dest(paths.mocks.dest.html));
});

/**
 * TASK: CLOSURE
 */

gulp.task('closure', ['clean:closure'], function () {
    return gulp.src([paths.closure.src.html])
        .pipe(soynode({renderSoyWeb: true}))
        .pipe(htmlbeautify(options.beautify))
        .pipe(cachebust(options.cache))
        .pipe(gulp.dest(paths.closure.dest.html));
});

/**
 * TASK: BROWSER
 */

gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: "assets/dest",
            directory: true
        },
        port: 3002,
        middleware: function(req,res,next) {
            if (req.headers.host === 'localhost:3002') {
                //automatic redirect to local.oci.com (must have correct HAProxy settings pointing /assets/v1 to 127.0.0.1:<port>)
                res.writeHead(302, {
                    'Location': 'http://local.oci.com' + req.url
                });
                res.end();
            } else {
                //remove /assets/v1 from incoming requests which will serve from baseDir (assets/dest)
                var pathToReplace = '/assets/v1';
                if (req.url.startsWith(pathToReplace)) {
                    req.url = req.url.length > pathToReplace.length ? req.url.substr(pathToReplace.length) : '/';
                }
            }
            return next();
        },
        startPath: "assets/v1"
    });
    return gulp.watch([
        paths.styles.dest.project + '/**/*.css',
        paths.scripts.dest.project + '/**/*.js',
        paths.mocks.dest.html + '/**/*.html',
        paths.closure.dest.html + '/**/*.html'
    ]).on("change", reload);
});

/**
 * TASK: WATCH
 */

gulp.task('watch-styles', function () {
    return gulp.watch([
        paths.styles.src.project + '/**/*.scss'
    ], ['styles:dev','styles:redesign']);
});

gulp.task('watch-scripts', function () {
    return gulp.watch([
        paths.scripts.src.project
    ], ['scripts:dev']);
});

gulp.task('watch-mocks', function () {
    return gulp.watch([
        paths.mocks.src.html
    ], ['mocks']);
});

gulp.task('watch-closure', function () {
    return gulp.watch([
        paths.closure.src.html
    ], ['closure']);
});

gulp.task('watch', ['watch-styles', 'watch-scripts', 'watch-mocks','watch-closure']);

/**
 * TASK: BUILD LIVE
 */

gulp.task('build:live', function() {
    return runSequence(
        'common:dev',
        'styles:dev',
        'styles:redesign',
        'scripts:dev',
        'mocks',
        'closure',
        'watch',
        'server'
    );
});

/**
 * TASK: BUILD DEV
 */

gulp.task('build:dev', function() {
    return runSequence(
        'common:dev',
        'styles:dev',
        'styles:redesign',
        'scripts:dev',
        'mocks',
        'closure'
    );
});

/**
 * TASK: BUILD PRO
 */

gulp.task('build:pro', function() {
    return runSequence(
        'common:pro',
        'styles:pro',
        'styles:redesign',
        'scripts:pro',
        'mocks',
        'closure'
    );
});

/**
 * TASK: DEFAULT
 */

gulp.task('default', ['build:dev']);